<?php

use App\Cd;
use App\Renter;
use App\Transaction;

class TransactionTest extends TestCase
{
    /**
     * /transaction [GET]
     */
    public function testShouldReturnAllTransactions(){

        $this->get("transaction", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'renter_id',
                    'cd_id',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ],
            'meta' => [
                '*' => [
                    'total',
                    'count',
                    'per_page',
                    'current_page',
                    'total_pages',
                    'links',
                ]
            ]
        ]);
        
    }

    /**
     * /transaction/id [GET]
     */
    public function testShouldReturnTransaction(){
        $this->get("transaction/1", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'renter_id',
                    'cd_id',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]    
        );
        
    }

    /**
     * /transaction [POST]
     */
    public function testShouldCreateTransaction(){

        $cd = Cd::find(1);
        $renter = Renter::find(1);

        $parameters = [
            'renter_id'    => $renter->id,
            'cd_id'        => $cd->id
        ];

        $this->post("transaction", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'renter_id',
                    'cd_id',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]    
        );
        
    }
    
    /**
     * /renter/id [DELETE]
     */
    public function testShouldCheckReturnTransaction(){
        
        $this->post("transaction/5/return", [], []);
        $transaction = Transaction::find(5);
        $this->assertNotNull($transaction->returned_at);
        $this->seeStatusCode(200);
    }

}