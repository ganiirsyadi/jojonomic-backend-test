<?php

class CdTest extends TestCase
{
    /**
     * /cd [GET]
     */
    public function testShouldReturnAllCds(){

        $this->get("cd", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'title',
                    'rate',
                    'category',
                    'quantity',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ],
            'meta' => [
                '*' => [
                    'total',
                    'count',
                    'per_page',
                    'current_page',
                    'total_pages',
                    'links',
                ]
            ]
        ]);
        
    }

    /**
     * /cd/id [GET]
     */
    public function testShouldReturnCd(){
        $this->get("cd/1", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'title',
                    'rate',
                    'category',
                    'quantity',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]    
        );
        
    }

    /**
     * /cd [POST]
     */
    public function testShouldCreateCd(){

        $parameters = [
            'title'     => 'Avanger',
            'rate'      => 10000,
            'category'  => 'Action',
            'quantity'  => 8
        ];

        $this->post("cd", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'title',
                    'rate',
                    'category',
                    'quantity',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]    
        );
        
    }
    
    /**
     * /cd/id [PUT]
     */
    public function testShouldUpdateCd(){

        $parameters = [
            'title'     => 'Insidious',
            'rate'      => 15000,
            'category'  => 'Horror',
            'quantity'  => 2
        ];

        $this->put("cd/1", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'title',
                    'rate',
                    'category',
                    'quantity',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]    
        );
    }

    /**
     * /cd/id [DELETE]
     */
    public function testShouldDeleteCd(){
        
        $this->delete("cd/3", [], []);
        $this->seeStatusCode(410);
        $this->seeJsonStructure([
                'status',
                'message'
        ]);
    }

}