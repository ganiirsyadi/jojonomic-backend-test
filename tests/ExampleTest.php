<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            "<h1>CD Rental API</h1><h4>Read the <u>APIBluePrint.md</u> for api usage</h4>" 
            . $this->app->version(), $this->response->getContent()
        );
    }
}
