<?php

class RenterTest extends TestCase
{
    /**
     * /renter [GET]
     */
    public function testShouldReturnAllRenters(){

        $this->get("renter", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'name',
                    'email',
                    'address',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ],
            'meta' => [
                '*' => [
                    'total',
                    'count',
                    'per_page',
                    'current_page',
                    'total_pages',
                    'links',
                ]
            ]
        ]);
        
    }

    /**
     * /renter/id [GET]
     */
    public function testShouldReturnRenter(){
        $this->get("renter/1", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'email',
                    'address',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]    
        );
        
    }

    /**
     * /renter [POST]
     */
    public function testShouldCreateRenter(){

        $parameters = [
            'name'     => 'Gani',
            'email'      => 'gani@jojonomic.com',
            'address'  => 'Depok, Jawa Barat',
        ];

        $this->post("renter", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'email',
                    'address',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]    
        );
        
    }
    
    /**
     * /renter/id [PUT]
     */
    public function testShouldUpdateRenter(){

        $parameters = [
            'name'     => 'Ilham',
            'email'     => 'ilham@jojonomic.com',
            'address'  => 'Depok, Jawa Barat',
        ];

        $this->put("renter/1", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'email',
                    'address',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]    
        );
    }

    /**
     * /renter/id [DELETE]
     */
    public function testShouldDeleteRenter(){
        
        $this->delete("renter/3", [], []);
        $this->seeStatusCode(410);
        $this->seeJsonStructure([
                'status',
                'message'
        ]);
    }

}