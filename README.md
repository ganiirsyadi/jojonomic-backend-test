# Jojonomic Backend Test

Using php and lumen framework to create an API. This project is the backend test for Jojonomic internship opportunity.

For api details, check the APIBluePrint.md

## Preparation

-   `git clone https://gitlab.com/ganiirsyadi/jojonomic-backend-test.git`
-   `cd jojonomic-backend-test`
-   Create an .env file and configure it,
    - APP_KEY : fill it with 32 random characters
    - DB_DATABASE : fill it with your database name
    - DB_USERNAME : fill it with your database username (default=root)
    - DB_PASSWORD : fill it with your database password (default=blank)
-   `composer install`

## Testing

-   `php artisan migrate:refresh`
-   `php artisan db:seed`
-   `vendor/bin/phpunit`

## Usage

-   `php artisan migrate:refresh`
-   `php -S localhost:8000 -t public`

