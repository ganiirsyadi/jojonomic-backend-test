FORMAT: 1A

# CdRental

CdRental is an API where consumers can rent and return cd from collection. It also help the owner to add / remove / edit / see current collection.

# Polls API Root [/]

This resource does not have any attributes. 

## Group CD

Resources related to cd in the API.

## CD [/cd/{cd_id}]

A Cd object has the following attributes:

+ title - A string of cd's title
+ rate - An integer of cd's rate (price) for rent / day
+ category - A string of cd's category
+ quantity - An integer of cd's quantity
+ created_at - A date with format (DD-MM-YYYY) when the cd was added to database.
+ updated_at - A date with format (DD-MM-YYYY) when the cd was updated.
+ links - An array of uri

+ Parameters
    + cd_id: 1 (required, number) - ID of the Cd in form of an integer

### View a Cd Detail [GET]

+ Response 200 (application/json)

        {
            "data": {
                "id": 1,
                "title": "Acanger",
                "rate": 25000,
                "category": "action",
                "quantity": 2,
                "created_at": "19-03-2020",
                "updated_at": "19-03-2020",
                "links": [
                    {
                        "uri": "cd\/1"
                    }
                ]
            }
        }

### Edit a Cd Details [PUT]

You may edit cd detail using this action. It takes a JSON object.

+ title (string) - Cd Title
+ rate (integer]) - Cd rate/price per day
+ category (string) - Cd category
+ quantity (integer) - Cd quantity (must be >= 0)

+ Parameters
    + cd_id: 1 (required, number) - ID of the Cd in form of an integer

+ Request (application/json)

        {
            "title": "Avanger",
            "rate": 15000,
            "category": "action",
            "quantity":20
        }

+ Response 200 (application/json)


        {
            "data": {
                "id": 1,
                "title": "ant man",
                "rate": 10000,
                "category": "action",
                "quantity": 15,
                "created_at": "19-03-2020",
                "updated_at": "19-03-2020",
                "links": [
                    {
                    "uri": "cd\/1"
                    }
                ]
            }
        }

### Delete a Cd [DELETE]

You may delete a cd from the collection using this action.

+ Parameters
    + cd_id: 1 (required, number) - ID of the Cd in form of an integer

+ Response 410 (application/json)

        {
            "status": 410,
            "message": "Cd deleted successfully!"
        }

## CD Collection [/cd{?page}]

+ Parameters
    + page: 1 (optional, number) - The page of cd to return

### List All Questions [GET]

+ Response 200 (application/json)

        {
            "data": [
                {
                    "id": 1,
                    "title": "ant man",
                    "rate": 10000,
                    "category": "action",
                    "quantity": 15,
                    "created_at": "19-03-2020",
                    "updated_at": "19-03-2020",
                    "links": [
                        {
                            "uri": "cd\/1"
                        }
                ]
            }],
            "meta": {
                "pagination": {
                "total": 29,
                "count": 1,
                "per_page": 15,
                "current_page": 1,
                "total_pages": 2,
                "links": {
                        "next": "http:\/\/localhost:8080\/cd?page=2"
                    }
                }
            }
        }

### Add a New Cd [POST]

You may add a cd to the collection using this action. It takes a JSON object.

+ title (string) - Cd Title
+ rate (integer]) - Cd rate/price per day
+ category (string) - Cd category
+ quantity (integer) - Cd quantity (must be >= 0)

+ Request (application/json)

        {
            "title": "Avanger",
            "rate": 15000,
            "category": "action",
            "quantity":20
        }

+ Response 200 (application/json)

        {
            "data": {
                "id": 2,
                "title": "Avanger",
                "rate": 15000,
                "category": "action",
                "quantity": 20,
                "created_at": "19-03-2020",
                "updated_at": "19-03-2020",
                "links": [
                    {
                    "uri": "cd\/1"
                    }
                ]
            }
        }

## Group Renter

Resources related to renter in the API. Renter is someone who want to rent a cd.

## Renter [/renter/{renter_id}]

A Renter object has the following attributes:

+ name - A string of renter name
+ email - A string of renter email
+ address - A string of renter address
+ created_at - A date with format (DD-MM-YYYY) when the renter was added to database.
+ updated_at - A date with format (DD-MM-YYYY) when the renter was updated.
+ links - An array of uri

+ Parameters
    + renter_id: 1 (required, number) - ID of the Renter in form of an integer

### View a Renter Detail [GET]

+ Response 200 (application/json)

        {
            "data": {
                "id": 1,
                "name": "Sydni Hilpert",
                "email": "brando60@keebler.net",
                "address": "977 Mazie Orchard\nVelvabury, RI 09288",
                "created_at": "19-03-2020",
                "updated_at": "19-03-2020",
                "links": [
                    {
                        "uri": "renter\/1"
                    }
                ]
            }
        }

### Edit a Renter Details [PUT]

You may edit renter detail using this action. It takes a JSON object.

+ name - A string of renter name
+ email - A string of renter email
+ address - A string of renter address

+ Parameters
    + renter_id: 1 (required, number) - ID of the Renter in form of an integer

+ Request (application/json)

        {
            "name": "Mr. Harmon Ebert",
            "email": "flo69@hotmail.com",
            "address": "2579 Delphine Lake Apt. 438\nRoselynshire, IN 88567-0146",        
        }

+ Response 200 (application/json)


        {
            "data": {
                "id": 1,
                "name": "Mr. Harmon Ebert",
                "email": "flo69@hotmail.com",
                "address": "2579 Delphine Lake Apt. 438\nRoselynshire, IN 88567-0146",
                "created_at": "19-03-2020",
                "updated_at": "19-03-2020",
                "links": [
                    {
                        "uri": "renter\/1"
                    }
                ]
            }
        }

### Delete a Renter [DELETE]

You may delete a Renter from the Renter collection using this action.

+ Parameters
    + cd_id: 1 (required, number) - ID of the Cd in form of an integer

+ Response 410 (application/json)

        {
            "status": 410,
            "message": "Renter deleted successfully!"
        }

## Renter Collection [/renter{?page}]

+ Parameters
    + page: 1 (optional, number) - The page of cd to return

### List All Renters [GET]

+ Response 200 (application/json)

        {
            "data": [
                {
                "id": 1,
                "name": "Mittie Ortiz",
                "email": "gino.frami@grimes.biz",
                "address": "2052 Ritchie Ports Suite 162\nJuliostad, MO 89010",
                "created_at": "19-03-2020",
                "updated_at": "19-03-2020",
                "links": [
                        {
                            "uri": "renter\/1"
                        }
                    ]
                }
            ],
            "meta": {
                "pagination": {
                "total": 20,
                "count": 1,
                "per_page": 15,
                "current_page": 1,
                "total_pages": 2,
                "links": {
                    "next": "http:\/\/localhost:8080\/renter?page=2"
                    }
                }
            }
        }


### Add a New Renter [POST]

You need to add renter before he/she can rent a cd using this action. It will return the renter id that will be used to make a transaction. It takes a JSON object.

+ name - A string of renter name
+ email - A string of renter email
+ address - A string of renter address

+ Request (application/json)

        {
            "name": "Mr. Harmon Ebert",
            "email": "flo69@hotmail.com",
            "address": "2579 Delphine Lake Apt. 438\nRoselynshire, IN 88567-0146",        
        }

+ Response 200 (application/json)

        {
            "data": {
                "id": 1,
                "name": "Mr. Harmon Ebert",
                "email": "flo69@hotmail.com",
                "address": "2579 Delphine Lake Apt. 438\nRoselynshire, IN 88567-0146",
                "created_at": "19-03-2020",
                "updated_at": "19-03-2020",
                "links": [
                    {
                        "uri": "renter\/1"
                    }
                ]
            }
        }

## Group Transaction

Resources related to transaction in the API. Transaction has table relationship with renter and cd. It belongsto (Many to one relationship) with renters and cds table.

## Transaction [/transaction/{transaction_id}]

A Transaction object has the following attributes:

+ renter_id - ID from the renter who make transaction (foreignKey).
+ cd_id - ID from the cd which is rented (foreignKey).
+ created_at - A date with format (DD-MM-YYYY) when the transaction was added to database.
+ returned_at - A date with format (DD-MM-YYYY) when the cd was returned (default=null).
+ links - An array of uri

+ Parameters
    + transaction_id: 1 (required, number) - ID of the trasaction in form of an integer

### View a Transaction Detail [GET]

+ Response 200 (application/json)

        {
            "data": {
                "id": 1,
                "renter_id": 1,
                "cd_id": 13,
                "created_at": "20-03-2020",
                "updated_at": "20-03-2020",
                "returned_at": null,
                "links": [
                    {
                        "uri": "transaction\/1"
                    }
                ]
            }
        }

## Finish a Transaction / Return the cd [/transaction/{transaction_id}/return]

### Finish a Transaction [POST]

You may finish a transaction with id transaction_id using this action.
This action fills the returned_at field with a date when this action performed.
It also increase the quantity of corresponding cd by 1 unit.

It will return a respone of a json contain the total price.
Total price is calculated using this formula (total day * rate)

*total day = returned_at - created_at

+ Parameters
    + renter_id: 1 (required, number) - ID of the Transaction in form of an integer

+ Response 200 (application/json)

        {
            "total_price": 0
        }

## Transaction Collection [/transaction{?page}]

+ Parameters
    + page: 1 (optional, number) - The page of Transaction to return

### List All Transaction [GET]

+ Response 200 (application/json)

        {
            "data": [
                {
                    "id": 1,
                    "renter_id": 1,
                    "cd_id": 13,
                    "created_at": "20-03-2020",
                    "updated_at": "20-03-2020",
                    "returned_at": "20-03-2020",
                    "links": [
                        {
                        "uri": "transaction\/1"
                        }
                    ]
                },
            ],
            "meta": {
                "pagination": {
                "total": 20,
                "count": 1,
                "per_page": 15,
                "current_page": 1,
                "total_pages": 2,
                "links": {
                    "next": "http:\/\/localhost:8080\/transaction?page=2"
                    }
                }
            }
        }

### Add a New Transaction [POST]

Renter can rent a cd using this action. The renter may use the renter_id to make a transaction of a cd with an id of cd_id. It takes a JSON object.

This action will also automatically reduce the quantity of corresponding cd by 1 unit. If there is no stock (quantity=0) it will return an error massage.

+ renter_id - ID from the renter who make transaction (foreignKey).
+ cd_id - ID from the cd which is rented (foreignKey).

+ Request (application/json)

        {
            "renter_id": 1,
            "cd_id": 1,
        }

+ Response 200 (application/json)

        {
            "data": {
                "id": 2,
                "renter_id": 1,
                "cd_id": 1,
                "created_at": "20-03-2020",
                "updated_at": "20-03-2020",
                "returned_at": null,
                "links": [
                    {
                        "uri": "transaction\/2"
                    }
                ]
            }
        }

+ Response 404 (application/json) -> if the quantity of the cd is 0

        {
            "status": 404,
            "message": "Cd out of stock"
        }