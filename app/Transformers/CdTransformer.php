<?php
namespace App\Transformers;

use App\Cd;
use League\Fractal;

class CdTransformer extends Fractal\TransformerAbstract
{
	public function transform(Cd $cd)
	{
	    return [
	        'id'            => (int) $cd->id,
	        'title'         => $cd->title,
            'rate'          => (int) $cd->rate,
            'category'      => $cd->category,
	        'quantity'      => (int) $cd->quantity,
	        'created_at'    => $cd->created_at->format('d-m-Y'),
	        'updated_at'    => $cd->updated_at->format('d-m-Y'),
            'links'   => [
                [
                    'uri' => 'cd/'.$cd->id,
                ]
            ],
	    ];
	}
}