<?php
namespace App\Transformers;

use App\Transaction;
use League\Fractal;

class TransactionTransformer extends Fractal\TransformerAbstract
{
	public function transform(Transaction $transaction)
	{
	    return [
	        'id'            => (int) $transaction->id,
	        'renter_id'     => (int) $transaction->renter_id,
	        'cd_id'   	  	=> (int) $transaction->cd_id,
	        'created_at'    => $transaction->created_at->format('d-m-Y'),
			'updated_at'    => $transaction->updated_at->format('d-m-Y'),
			'returned_at'	=> $transaction->returned_at != null ? date_format(date_create($transaction->returned_at),"d-m-Y") : null,
            'links'   => [
                [
                    'uri' => 'transaction/'.$transaction->id,
                ]
            ],
	    ];
	}
}