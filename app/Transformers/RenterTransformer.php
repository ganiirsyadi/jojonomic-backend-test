<?php
namespace App\Transformers;

use App\Renter;
use League\Fractal;

class RenterTransformer extends Fractal\TransformerAbstract
{
	public function transform(Renter $renter)
	{
	    return [
	        'id'            => (int) $renter->id,
	        'name'          => $renter->name,
            'email'         => $renter->email,
            'address'       => $renter->address,
	        'created_at'    => $renter->created_at->format('d-m-Y'),
	        'updated_at'    => $renter->updated_at->format('d-m-Y'),
            'links'   => [
                [
                    'uri' => 'renter/'.$renter->id,
                ]
            ],
	    ];
	}
}