<?php

namespace App;

use App\Cd;
use App\Renter;


use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'renter_id', 'cd_id'
    ];
 
    
    public function cd()
    {
        return $this->belongsTo(Cd::class);
    }
    public function renter()
    {
        return $this->belongsTo(Renter::class);
    }

}
