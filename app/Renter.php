<?php

namespace App;
use App\Transaction;

use Illuminate\Database\Eloquent\Model;

class Renter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'address'
    ];

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

}
