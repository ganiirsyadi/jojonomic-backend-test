<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Cd;

use Illuminate\Http\Request;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\TransactionTransformer;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $fractal;

    public function __construct()
    {
        $this->fractal = new Manager();  
    }
    /**
     * GET /transaction
     * 
     * @return array
     */
    public function index(){
        $paginator = Transaction::paginate();
        $transaction = $paginator->getCollection();
        $resource = new Collection($transaction, new TransactionTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return $this->fractal->createData($resource)->toArray();
    }

    public function show($id){
        $transaction = Transaction::find($id);
        $resource = new Item($transaction, new TransactionTransformer);
        return $this->fractal->createData($resource)->toArray();
    }

    public function store(Request $request){

        $cd = Cd::findOrFail($request['cd_id']);
        if($cd->quantity == 0){
            return $this->customResponse('Cd out of stock', 404);
        }
        $cd->quantity -= 1;
        $cd->save();

        //validate request parameters

        $transaction = Transaction::create($request->all());
        $resource = new Item($transaction, new TransactionTransformer);
        return $this->fractal->createData($resource)->toArray();
    }


    public function return($id){
        
        //Return error 404 response if transaction was not found
        if(!Transaction::find($id)) return $this->errorResponse('Transaction not found!', 404);

        //Return 410(done) success response if return was successful
        if(Transaction::find($id)){
            $transaction = Transaction::find($id);
            $transaction->returned_at = date_create();
            $totalDay = date_diff(date_create($transaction->created_at), $transaction->returned_at);
            $cd = Cd::find($transaction->cd_id);
            $cd->quantity += 1;
            $cd->save();
            $transaction->save();
            $totalPrice = (int) $cd->rate * (int) $totalDay->format('%a');
            return response()->json(['total_price' => $totalPrice]);
        }

        //Return error 400 response if return was not successful
        return $this->errorResponse('Failed to return cd!', 400);
    }

    public function customResponse($message = 'success', $status = 200)
    {
        return response(['status' =>  $status, 'message' => $message], $status);
    }
}