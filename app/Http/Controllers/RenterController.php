<?php

namespace App\Http\Controllers;

use App\Renter;
use Illuminate\Http\Request;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\RenterTransformer;

class RenterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $fractal;

    public function __construct()
    {
        $this->fractal = new Manager();  
    }
    /**
     * GET /renter
     * 
     * @return array
     */
    public function index(){
        $paginator = Renter::paginate();
        $renter = $paginator->getCollection();
        $resource = new Collection($renter, new RenterTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return $this->fractal->createData($resource)->toArray();
    }

    public function show($id){
        $renter = Renter::find($id);
        $resource = new Item($renter, new RenterTransformer);
        return $this->fractal->createData($resource)->toArray();
    }

    public function store(Request $request){

        //validate request parameters
        $this->validate($request, [
            'name' => 'bail|required|max:255',
            'email' => 'bail|required|max:255',
            'address' => 'required',
        ]);

        $renter = Renter::create($request->all());
        $resource = new Item($renter, new RenterTransformer);
        return $this->fractal->createData($resource)->toArray();
    }

    public function update($id, Request $request){

        //validate request parameters
        $this->validate($request, [
            'name' => 'bail|required|max:255',
            'email' => 'bail|required|max:255',
            'address' => 'required',
        ]);

        //Return error 404 response if renter was not found
        if(!Renter::find($id)) return $this->errorResponse('Renter not found!', 404);

        $renter = Renter::find($id)->update($request->all());

        if($renter){
            //return updated data
            $resource = new Item(Renter::find($id), new RenterTransformer); 
            return $this->fractal->createData($resource)->toArray();
        }

        //Return error 400 response if updated was not successful        
        return $this->errorResponse('Failed to update renter!', 400);
    }

    public function destroy($id){
        
        //Return error 404 response if renter was not found
        if(!Renter::find($id)) return $this->errorResponse('Renter not found!', 404);

        //Return 410(done) success response if delete was successful
        if(Renter::find($id)->delete()){
            return $this->customResponse('Renter deleted successfully!', 410);
        }

        //Return error 400 response if delete was not successful
        return $this->errorResponse('Failed to delete renter!', 400);
    }

    public function customResponse($message = 'success', $status = 200)
    {
        return response(['status' =>  $status, 'message' => $message], $status);
    }
}