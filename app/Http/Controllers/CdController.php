<?php

namespace App\Http\Controllers;

use App\Cd;
use Illuminate\Http\Request;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\CdTransformer;

class CdController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $fractal;

    public function __construct()
    {
        $this->fractal = new Manager();  
    }
    /**
     * GET /cd
     * 
     * @return array
     */
    public function index(){
        $paginator = Cd::paginate();
        $cd = $paginator->getCollection();
        $resource = new Collection($cd, new CdTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return $this->fractal->createData($resource)->toArray();
    }

    public function show($id){
        $cd = Cd::find($id);
        $resource = new Item($cd, new CdTransformer);
        return $this->fractal->createData($resource)->toArray();
    }

    public function store(Request $request){

        //validate request parameters
        $this->validate($request, [
            'title' => 'bail|required|max:255',
            'rate' => 'bail|required|min:0',
            'category' => 'bail|required|max:50',
            'quantity' => 'required|integer|min:0'
        ]);

        $cd = Cd::create($request->all());
        $resource = new Item($cd, new CdTransformer);
        return $this->fractal->createData($resource)->toArray();
    }

    public function update($id, Request $request){

        //validate request parameters
        $this->validate($request, [
            'title' => 'bail|required|max:255',
            'rate' => 'bail|required|min:0',
            'category' => 'bail|required|max:50',
            'quantity' => 'required|integer|min:0'
        ]);

        //Return error 404 response if cd was not found
        if(!Cd::find($id)) return $this->errorResponse('Cd not found!', 404);

        $cd = Cd::find($id)->update($request->all());

        if($cd){
            //return updated data
            $resource = new Item(Cd::find($id), new CdTransformer); 
            return $this->fractal->createData($resource)->toArray();
        }

        //Return error 400 response if updated was not successful        
        return $this->errorResponse('Failed to update cd!', 400);
    }

    public function destroy($id){
        
        //Return error 404 response if cd was not found
        if(!Cd::find($id)) return $this->errorResponse('Cd not found!', 404);

        //Return 410(done) success response if delete was successful
        if(Cd::find($id)->delete()){
            return $this->customResponse('Cd deleted successfully!', 410);
        }

        //Return error 400 response if delete was not successful
        return $this->errorResponse('Failed to delete cd!', 400);
    }

    public function customResponse($message = 'success', $status = 200)
    {
        return response(['status' =>  $status, 'message' => $message], $status);
    }
}