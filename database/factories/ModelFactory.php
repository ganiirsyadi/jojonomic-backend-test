<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Cd::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'rate' => $faker->numberBetween($min=5, $max=50)*1000,
        'category' => $faker->word,
        'quantity' => $faker->numberBetween($min=0, $max=15)
    ];
});

$factory->define(App\Renter::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'address' => $faker->address,
    ];
});

$factory->define(App\Transaction::class, function (Faker $faker) {

    $renter = App\renter::all()->pluck('id')->toArray();
    $cd = App\cd::all()->pluck('id')->toArray();

    return [
        'renter_id' => $faker->randomElement($renter),
        'cd_id' => $faker->randomElement($cd),
    ];
});


