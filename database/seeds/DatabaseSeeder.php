<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        factory(App\Cd::class, 30)->create();
        factory(App\Renter::class, 20)->create();
        factory(App\Transaction::class, 20)->create();
    }
}
