<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "<h1>CD Rental API</h1><h4>Read the <u>APIBluePrint.md</u> for api usage</h4>" 
    . $router->app->version();
});

// api end-point for Cd


$router->get('cd', 'CdController@index');
$router->get('cd/{id}', 'CdController@show');
$router->post('cd', 'CdController@store');
$router->put('cd/{id}', 'CdController@update');
$router->delete('cd/{id}', 'CdController@destroy');


// api end-point for Renter Model

$router->get('renter', 'RenterController@index');
$router->get('renter/{id}', 'RenterController@show');
$router->post('renter', 'RenterController@store');
$router->put('renter/{id}', 'RenterController@update');
$router->delete('renter/{id}', 'RenterController@destroy');


// api end-point for Transaction Model

$router->get('transaction', 'TransactionController@index');
$router->get('transaction/{id}', 'TransactionController@show');

// Rent a cd
$router->post('transaction', 'TransactionController@store');
// Return a cd
$router->post('transaction/{id}/return', 'TransactionController@return');
